COMPOSER = /usr/local/bin/composer
DASHING = /Users/abackstrom/bin/dashing

.PHONY: prep external clean

all: prep external output/index.html
	$(DASHING) build -s output
	cp -v icons/icon*.png annika.docset/
	cp -v style.css annika.docset/Contents/Resources/Documents/

prep:
	$(COMPOSER) install

external:
	php build.php "/Users/abackstrom/Dropbox/Etsy/notes/Projects.md" > output/projects.html
	php build.php "/Users/abackstrom/Dropbox/Etsy/notes/rodeo-users-guide.md" > output/rodeo-users-guide.html
	php build.php "/Users/abackstrom/Dropbox/Etsy/notes/mobile-browser-testing-guidelines.md" > output/mobile-browser-testing-guidelines.html
	php build.php "/Users/abackstrom/Dropbox/Etsy/notes/trust-safety-company-reads.md" > output/trust-safety-company-reads.html

output/index.html: prep pages/*.html
	php build.php > output/index.html

clean:
	rm -rfv output/*.html annika.docset
