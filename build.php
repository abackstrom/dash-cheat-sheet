<?php

require_once __DIR__ . '/vendor/autoload.php';

ini_set('display_errors', 'stderr');
error_reporting(E_ALL);

use League\CommonMark\CommonMarkConverter;

$converter = new CommonMarkConverter();

$loader = new Twig_Loader_Filesystem(__DIR__ . '/pages');
$twig = new Twig_Environment($loader);

class Cheat_Extension extends Twig_Extension {
    public function getFilters() {
        return array(
            new Twig_SimpleFilter('function_signature', array($this, 'filterFunction'), array('is_safe' => array('html'))),
        );
    }

    public function filterFunction($signature) {
        $signature = preg_replace('/\s/', '', $signature);
        preg_match('/^(?P<func>\w+)(?:\((?P<args>.*)\))?$/', $signature, $matches);

        $func = $matches['func'];
        $args = isset($matches['args']) ? $matches['args'] : null;

        $args = $args ? explode(',', $args) : [];

        $args = array_map(function($arg) {
            $parts = preg_split('/([$\.&]+)/', $arg, 2, PREG_SPLIT_DELIM_CAPTURE);

            if ($parts[0]) {
                $type = $parts[0];
                $name = $parts[1] . $parts[2];
                return sprintf('<span class="s-type">%s</span> <span class="s-arg">%s</span>', htmlentities($type), htmlentities($name));
            }

            $name = $parts[1] . $parts[2];
            return sprintf('<span class="s-arg">%s</span>', htmlentities($name));
        }, $args);

        $args = $args ? implode(', ', $args) : '';

        return sprintf('<span class="s-func">%s</span> ( %s )', htmlentities($func), $args);
    }
}

$twig->addExtension(new Cheat_Extension);

$args = $argv;
array_shift($args); // remove command

if (count($args) > 1) {
    throw new RuntimeException("I take only 0 or 1 arguments.");
}

if (count($args) == 1) {
    $external_file = array_shift($args);
    $output = 'output/' . basename(strtolower($external_file), ".md") . ".html";

    $markdown = file_get_contents($external_file);
    $html = $converter->convertToHtml($markdown);
    echo $twig->render('external.html', array(
        'html' => $html,
    ));
    exit(0);
}

// fallback
echo $twig->render('index.html');
