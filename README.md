# Annika Cheat Sheet (Dash)

A project that turns a simple one-page "cheat sheet" into a
[Dash](https://kapeli.com/dash) docset, with section markers.

![Screenshot of Cheat Sheet](screenshot.png)

## Requirements

* [Dashing](https://github.com/technosophos/dashing)

## Building

    $ vim index.html
    ... make changes ...
    $ make

## Notes

Double-click the resulting `.docset` file to add the docset to Dash. Dash will
reference that package in the future (rather than importing a copy somewhere).
To modify the docset, edit index.html and style.css and ⌘-R in Dash to see your
changes.

## Future work

* Open external links using JavaScript: `window.dash.openExternal_('https://google.com/');`
